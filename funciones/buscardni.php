<?php

include_once '../config/conexion.php';

$return_arr = array();

if ($conexion) {
    $fetch = mysqli_query(
         $conexion,
        "SELECT * FROM apoderado WHERE dni like '%" .
            mysqli_real_escape_string($conexion, $_GET['term']) .
            "%' LIMIT 0 ,50"
    );

    while ($row = mysqli_fetch_array($fetch)) {
        $num_documento = $row['dni'];
        $row_array['value'] =
            $row['dni'] .
            " | " .
            $row['nombres'] .
            "|" .
            $row['apellidopaterno'] .
            "|" .
            $row['apellidomaterno'];
        $row_array['idApoderado'] = $row['id'];
        $row_array['dniApoderado'] = $row['dni'];
        $row_array['nombreApoderado'] = $row['nombres'];
        $row_array['apePaternoApoderado'] = $row['apellidopaterno'];
        $row_array['apeMaternoApoderado'] = $row['apellidomaterno'];
        array_push($return_arr, $row_array);
    }
}
/* Cierra la conexión. */
mysqli_close($conexion);

/* Codifica el resultado del array en JSON. */
echo json_encode($return_arr);

?>