﻿--
-- Script was generated by Devart dbForge Studio 2019 for MySQL, Version 8.1.45.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 5/03/2020 16:18:58
-- Server version: 5.7.29-0ubuntu0.16.04.1
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS policinico;

CREATE DATABASE policinico
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Set default database
--
USE policinico;

--
-- Create table `gradoinstruccion`
--
CREATE TABLE gradoinstruccion (
  id int(11) NOT NULL AUTO_INCREMENT,
  gradoinstruccion varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 5,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `distritos`
--
CREATE TABLE distritos (
  id int(11) NOT NULL COMMENT 'id unico',
  distritos varchar(50) NOT NULL COMMENT 'nombre de distrito',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 655,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `sexo`
--
CREATE TABLE sexo (
  id int(11) NOT NULL AUTO_INCREMENT,
  sexo varchar(20) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `apoderado`
--
CREATE TABLE apoderado (
  id int(11) NOT NULL AUTO_INCREMENT,
  apellidopaterno varchar(50) DEFAULT NULL,
  apellidomaterno varchar(50) DEFAULT NULL,
  nombres varchar(60) DEFAULT NULL,
  dni varchar(20) DEFAULT NULL,
  idgradoinstruccion int(11) DEFAULT NULL,
  iddistrito int(11) DEFAULT NULL,
  direccion varchar(255) DEFAULT NULL,
  sector varchar(500) DEFAULT NULL,
  celular varchar(30) DEFAULT NULL,
  email varchar(100) DEFAULT NULL,
  idsexo int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 24,
AVG_ROW_LENGTH = 1170,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE apoderado
ADD CONSTRAINT FK_apoderado_distritos_id FOREIGN KEY (iddistrito)
REFERENCES distritos (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Create foreign key
--
ALTER TABLE apoderado
ADD CONSTRAINT FK_apoderado_gradoinstruccion_id FOREIGN KEY (idgradoinstruccion)
REFERENCES gradoinstruccion (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Create foreign key
--
ALTER TABLE apoderado
ADD CONSTRAINT FK_apoderado_sexo_id FOREIGN KEY (idsexo)
REFERENCES sexo (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Create table `seguro`
--
CREATE TABLE seguro (
  id int(11) NOT NULL AUTO_INCREMENT,
  seguro char(50) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `parentesco`
--
CREATE TABLE parentesco (
  id int(11) NOT NULL AUTO_INCREMENT,
  parentesco varchar(20) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 3276,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `nino`
--
CREATE TABLE nino (
  id int(11) NOT NULL AUTO_INCREMENT,
  historiaclinica varchar(50) DEFAULT NULL,
  apellidopaterno varchar(50) DEFAULT NULL,
  apellidomaterno varchar(50) DEFAULT NULL,
  nombres varchar(60) DEFAULT NULL,
  dni varchar(20) DEFAULT NULL,
  fechanacimineto date DEFAULT NULL,
  idseguro int(11) DEFAULT NULL,
  idsexo int(11) DEFAULT NULL,
  idapoderado int(11) DEFAULT NULL,
  idparentesco int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE nino
ADD CONSTRAINT FK_nino_apoderado_id FOREIGN KEY (idapoderado)
REFERENCES apoderado (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Create foreign key
--
ALTER TABLE nino
ADD CONSTRAINT FK_nino_parentesco_id FOREIGN KEY (idparentesco)
REFERENCES parentesco (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Create foreign key
--
ALTER TABLE nino
ADD CONSTRAINT FK_nino_seguro_id FOREIGN KEY (idseguro)
REFERENCES seguro (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Create foreign key
--
ALTER TABLE nino
ADD CONSTRAINT FK_nino_sexo_id FOREIGN KEY (idsexo)
REFERENCES sexo (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- 
-- Dumping data for table gradoinstruccion
--
INSERT INTO gradoinstruccion VALUES
(1, 'Primaria incompleta'),
(2, 'Primaria completa'),
(3, 'Secundaria incompleta'),
(4, 'Secundaria completa');

-- 
-- Dumping data for table distritos
--
INSERT INTO distritos VALUES
(1338, 'ANCON'),
(1339, 'ATE'),
(1340, 'BARRANCO'),
(1341, 'BREÑA'),
(1342, 'CARABAYLLO'),
(1343, 'CHACLACAYO'),
(1344, 'CHORRILLOS'),
(1345, 'CIENEGUILLA'),
(1346, 'COMAS'),
(1347, 'EL AGUSTINO'),
(1348, 'INDEPENDENCIA'),
(1349, 'JESUS MARIA'),
(1350, 'LA MOLINA'),
(1351, 'LA VICTORIA'),
(1352, 'LIMA'),
(1353, 'LINCE'),
(1354, 'LOS OLIVOS'),
(1355, 'LURIGANCHO'),
(1356, 'LURIN'),
(1357, 'MAGDALENA DEL MAR'),
(1358, 'MAGDALENA VIEJA'),
(1359, 'MIRAFLORES'),
(1360, 'PACHACAMAC'),
(1361, 'PUCUSANA'),
(1362, 'PUENTE PIEDRA'),
(1363, 'PUNTA HERMOSA'),
(1364, 'PUNTA NEGRA'),
(1365, 'RIMAC'),
(1366, 'SAN BARTOLO'),
(1367, 'SAN BORJA'),
(1368, 'SAN ISIDRO'),
(1369, 'SAN JUAN DE LURIGANCHO'),
(1370, 'SAN JUAN DE MIRAFLORES'),
(1371, 'SAN LUIS'),
(1372, 'SAN MARTIN DE PORRES'),
(1373, 'SAN MIGUEL'),
(1374, 'SANTA ANITA'),
(1375, 'SANTA MARIA DEL MAR'),
(1376, 'SANTA ROSA'),
(1377, 'SANTIAGO DE SURCO'),
(1378, 'SURQUILLO'),
(1379, 'VILLA EL SALVADOR'),
(1380, 'VILLA MARIA DEL TRIUNFO');

-- 
-- Dumping data for table sexo
--
INSERT INTO sexo VALUES
(1, 'Femenino'),
(2, 'Masculino');

-- 
-- Dumping data for table seguro
--
INSERT INTO seguro VALUES
(1, 'SIS'),
(2, 'ESSALUD'),
(3, 'RIMAC');

-- 
-- Dumping data for table parentesco
--
INSERT INTO parentesco VALUES
(1, 'Madre'),
(2, 'Padre'),
(3, 'Tio'),
(4, 'Tia'),
(5, 'Hermanos'),
(6, 'Madrina');

-- 
-- Dumping data for table apoderado
--
INSERT INTO apoderado VALUES
(9, 'Quispe', 'Mamani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Quispe', 'Mamani', 'Andres', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Salazar', 'Avila', 'Hamner', '27073123', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Silva', 'Chavez', 'Alex', '27073126', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'Salazar', 'Avila', 'Glenis', '27073125', 2, 1354, NULL, NULL, NULL, NULL, NULL),
(14, 'Machuca', 'Agular', 'Esther', '27073156', 2, 1339, 'Av. Panamerica', NULL, NULL, NULL, NULL),
(15, 'Salazar', 'Silva', 'Rodolfo', '27073158', 4, 1372, 'Av. 17', 'Sector c', NULL, NULL, NULL),
(16, 'Monroy', 'benavides', 'Cesar antonio', '27073126', 4, 1377, 'Av. Jorge Chavez', 'Sector 5', '995069463', NULL, NULL),
(18, 'Alayo', 'mamani', 'carlos', '27073120', 2, 1340, 'Av. Lima', 'sin sector', '99999506', 'alayo@hotmail.com', NULL),
(19, 'mamani', 'huaman', 'henry', '27073126', 2, 1339, 'av. carabayllo', 'sector sin fin', '995069458', 'MAMANI@HOTMAIL.COM', 2),
(20, 'MAMANI', 'MAMANI', 'ROBERTO', '27073556', 4, 1340, 'AV. LIMA', 'SIN SECTOR', '995069856', 'MAMANI@GMAIL.COM', 2),
(21, 'Quispe', 'pachacutec', 'juan', '27073125', 1, 1356, 'av. central', 'sector 8', '995069468', 'quispe@hotmail.com', 1),
(22, 'Moreno', 'guadamor', 'juan', '76729288', 4, 1372, 'av. mariategui 278', 'sector olvidados de dios', '999331177', 'juan@hotmail.com', 2),
(23, 'de la puente', 'villacorta', 'andres', '76729266', 4, 1354, 'Av. carlos izaguirre 278', 'sin sector', '979252636', 'andres@gmail.com', 2);

-- 
-- Dumping data for table nino
--
INSERT INTO nino VALUES
(1, '000000001', 'Rojas', 'cateriano', 'luis', '27073126', '2015-12-15', 1, 2, 22, 2),
(2, '0000000002', 'salazar', 'del aguila', 'maria camila', '27073125', '2009-09-18', 2, 1, 11, 2);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;