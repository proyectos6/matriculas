<?php require_once "config/conexion.php"; ?>

<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/jquery-ui/jquery-ui.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="plugins/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="css/estilos.css">


    <title>Register | Flack</title>
</head>

<body>
    <div class="container">
        <div class="row mt-4">
            <div class="col-md">
                <img src="img/logo.jpg" width="200px">
            </div>
        </div>
        <div class="row cabezera mt-4">
            <div class="col-md">
                <p class="titulo">Registro de niños de 6 años atendidos en el establecimiento de salud de Jesús María
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md mt-4 mb-4">
                <h2 class="text-uppercase">Datos del niño</h2>
            </div>
        </div>
        <form id="frmRegistranino" name="frmRegistranino" method="post">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label>N° de la historia Clínica</label>
                                <input type="text" class="form-control text-uppercase" id="historiaClinica"
                                    name="historiaClinica" placeholder="EJ. 123456789">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Apellido Paterno</label>
                                <input type="text" class="form-control text-uppercase" id="apellidoParternoNino"
                                    name="apellidoParternoNino" placeholder="EJ. Quispe">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Apellido Materno</label>
                                <input type="text" class="form-control text-uppercase" id="apellidoMaternonoNino"
                                    name="apellidoMaternonoNino" placeholder="EJ. Mamani">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Nombres</label>
                                <input type="text" class="form-control text-uppercase" id="nombresNino"
                                    name="nombresNino" placeholder="EJ. Juan">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label>DNI</label>
                                <input type="number" class="form-control text-uppercase" id="dniNino" name="dniNino"
                                    placeholder="EJ. 27073156">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Fecha de nacimiento</label>
                                <input type="date" class="form-control text-uppercase" id="fechaNacNino"
                                    name="fechaNacNino" placeholder="EJ. dd/mm/aa">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Tipo de seguro</label>
                                <select class="custom-select text-uppercase" id="tipoSeguroNino" name="tipoSeguroNino">
                                    <option value="0" selected>Seleccionar</option>
                                    <?php
                                    $sql = "SELECT id, seguro FROM seguro";
                                    $query = $conexion->query($sql);
                                    while ($valores = mysqli_fetch_array($query)) {
                                        echo "<option value='" .
                                            $valores['id'] .
                                                "'>" .
                                            $valores['seguro'] .
                                             "</option>";
                                            }
                                            ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Sexo</label>
                                <select class="custom-select text-uppercase" id="sexoNino" name="sexoNino">
                                    <option value="0">Seleccionar</option>
                                    <?php
                                    $sql = "SELECT id, sexo FROM sexo";
                                    $query = $conexion->query($sql);
                                    while ($valores = mysqli_fetch_array($query)) {
                                        echo "<option value='" .
                                            $valores['id'] .
                                                "'>" .
                                            $valores['sexo'] .
                                             "</option>";
                                            }
                                            ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md d-none">
                            <div class="form-group">
                                <label>Id Apoderado</label>
                                <input type="text" class="form-control text-uppercase" id="idApoderado"
                                    name="idApoderado" placeholder="EJ. ID">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>DNI Apoderado</label>
                                <input type="number" class="form-control text-uppercase" id="dniApoderado"
                                    name="dniApoderado" placeholder="EJ. 27073126">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Nombre Apoderado</label>
                                <input type="text" class="form-control text-uppercase" id="nombreApoderado"
                                    name="nombreApoderado" placeholder="EJ. Juan">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Ape Pate Apode</label>
                                <input type="text" class="form-control text-uppercase" id="apePaternoApoderado"
                                    name="apePaternoApoderado" placeholder="EJ. Manrique">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Ape Mate Apode</label>
                                <input type="text" class="form-control text-uppercase" id="apeMaternoApoderado"
                                    name="apeMaternoApoderado" placeholder="EJ. Gonzales">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="">Parentesco</label>
                                <select class="form-control custom-select text-uppercase" id="parentescoNino"
                                    name="parentescoNino">
                                    <option value="0">Seleccionar</option>
                                    <?php
                                    $sql = "SELECT id, parentesco FROM parentesco";
                                    $query = $conexion->query($sql);
                                    while ($valores = mysqli_fetch_array($query)) {
                                        echo "<option value='" .
                                            $valores['id'] .
                                                "'>" .
                                            $valores['parentesco'] .
                                             "</option>";
                                            }
                                            ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md text-center">
                    <button type="button" class="btn botones" id="guardarNino">Guardar</button>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md mt-4 mb-4">
                <h2 class="text-uppercase">Datos del apoderado</h2>
            </div>
        </div>
        <form id="frmRegistra" name="frmRegistra" method="post">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label>Apellido Paterno</label>
                                <input type="text" class="form-control text-uppercase" id="apellidosApe"
                                    name="apellidosApe" placeholder="Ej: Quispe">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Apellido Materno</label>
                                <input type="text" class="form-control text-uppercase" id="apellidosMate"
                                    name="apellidosMate" placeholder="Ej: Mamani">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Nombres</label>
                                <input type="text" class="form-control text-uppercase" id="nombresApo" name="nombresApo"
                                    placeholder="Ej: Andres">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label>DNI</label>
                                <input type="number" class="form-control text-uppercase" id="dni" name="dni"
                                    placeholder="Ej: 27073126">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="">Grado de instruccion</label>
                                <select class="form-control custom-select text-uppercase" id="gradoInstgruccion"
                                    name="gradoInstgruccion">
                                    <option value="0">Seleccionar</option>
                                    <?php
                                    $sql = "SELECT id, gradoinstruccion FROM gradoinstruccion";
                                    $query = $conexion->query($sql);
                                    while ($valores = mysqli_fetch_array($query)) {
                                        echo "<option value='" .
                                            $valores['id'] .
                                                "'>" .
                                            $valores['gradoinstruccion'] .
                                             "</option>";
                                            }
                                            ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="">Distrito</label>
                                <select class="form-control custom-select text-uppercase" id="distrito" name="distrito">
                                    <option value="0">Seleccionar </option>
                                    <?php
                                 $sql = "SELECT id, distritos FROM distritos";
                                    $query = $conexion->query($sql);
                                    while ($valores = mysqli_fetch_array($query)) {
                                                echo "<option value='" .
                                                    $valores['id'] .
                                                    "'>" .
                                                    $valores['distritos'] .
                                                    "</option>";
                                            }
                            ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Direccion</label>
                                <input type="text" class="form-control text-uppercase" id="direccion" name="direccion"
                                    placeholder="Ej: Av. Mariategui">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label>Sector</label>
                                <input type="text" class="form-control text-uppercase" id="sector" name="sector"
                                    placeholder="Ej: Av. Sector C">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Celular</label>
                                <input type="tel" class="form-control text-uppercase" id="celular" name="celular"
                                    placeholder="Ej: 999999999">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control text-uppercase" id="email" name="email"
                                    placeholder="Ej: hola@hotmail.com">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="">Sexo</label><br>
                                <select class="form-control custom-select text-uppercase" id="sexo" name="sexo">
                                    <option value="0">Seleccionar</option>
                                    <?php
                                $sql = "SELECT id, sexo FROM sexo";
                                $query = $conexion->query($sql);
                                    while ($valores = mysqli_fetch_array($query)) {
                                         echo "<option value='" .
                                                $valores['id'] .
                                                "'>" .
                                                 $valores['sexo'] .
                                                "</option>";
                                            }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row mt-4 mb-4">
                <div class="col text-center">
                    <button type="button" class="btn botones" id="registrarDatos">Guardar</button>
                </div>
            </div>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <!-- <script src="plugins/jquery/jquery-3.4.1.min.js"></script> -->

    <script src="plugins/popper/popper.min.js"></script>

    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="js/script.js"></script>

    <script src="plugins/sweetalert2/sweetalert2.min.js"></script>
</body>

</html>