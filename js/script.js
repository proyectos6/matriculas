//buscar DNI del Apoderado
$(document).ready(function() {
  $("#dniApoderado").autocomplete({
    source: "funciones/buscardni.php",
    minLength: 2,
    select: function(event, ui) {
      event.preventDefault();
      $("#idApoderado").val(ui.item.idApoderado);
      $("#dniApoderado").val(ui.item.dniApoderado);
      $("#nombreApoderado").val(ui.item.nombreApoderado);
      $("#apePaternoApoderado").val(ui.item.apePaternoApoderado);
      $("#apeMaternoApoderado").val(ui.item.apeMaternoApoderado);
    }
  });
});

//funcion que desabilita nombre y apellidos del apoderado al momento de guardar niño

$(function() {
  document.getElementById("nombreApoderado").disabled = true;
  document.getElementById("apePaternoApoderado").disabled = true;
  document.getElementById("apeMaternoApoderado").disabled = true;
});

//funcion guardar niño
$(document).ready(function() {
  $("#guardarNino").click(function() {
    registranino();
  });
});

function registranino() {
  const expHistoriaClinica = /^\d{9}$/;

  const historiaClinica = $("#historiaClinica").val();
  const apellidoParternoNino = $("#apellidoParternoNino").val();
  const apellidoMaternonoNino = $("#apellidoMaternonoNino").val();
  const nombresNino = $("#nombresNino").val();
  const dniNino = $("#dniNino").val();
  const fechaNacNino = $("#fechaNacNino").val();
  const tipoSeguroNino = $("#tipoSeguroNino").val();
  const sexoNino = $("#sexoNino").val();
  const dniApoderado = $("#dniApoderado").val();
  const parentescoNino = $("#parentescoNino").val();

  let frmRegistranino = $("#frmRegistranino").serialize();

  if (!expHistoriaClinica.test(historiaClinica)) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese la historia clinica!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (apellidoParternoNino == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese el apellido paterno!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (apellidoMaternonoNino == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese el apellido materno!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (nombresNino == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese el nombre!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (dniNino == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese el DNI!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (fechaNacNino == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese la fecha de nacimiento!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (tipoSeguroNino == 0 || tipoSeguroNino == null) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Seleccione un tipo de seguro!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (sexoNino == 0 || sexoNino == null) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Seleccione el sexo!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (dniApoderado == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "ingrese el DNI del apoderado!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (parentescoNino == 0 || parentescoNino == null) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "ingrese el parentesco con el niño!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else {
    $.ajax({
      type: "POST",
      url: "funciones/registranino.php",
      data: frmRegistranino,
      success: function(r) {
        if (r == 2) {
          Swal.fire({
            type: "error",
            title: "Error",
            text: "niño ya existe",
            confirmButtonText: "Aceptar!"
          });
        } else if (r == 1) {
          Swal.fire({
            position: "top-end",
            type: "success",
            title: "Se registro con exito",
            showConfirmButton: false,
            timer: 1500
          });
          document.getElementById("frmRegistranino").reset();
        } else {
          Swal.fire({
            type: "error",
            title: "Error",
            text: "Apoderado no existe registrelo",
            confirmButtonText: "Aceptar!"
          });
        }
      }
    });
  }
}

//Funciones Guardar Apoderado
$(document).ready(function() {
  $("#registrarDatos").click(function() {
    registra();
  });
});

function registra() {
  const expdni = /^\d{8}$/;
  const expreCelu = /^\d{9}$/;
  const expreEmail = /\w+@\w+\.+[a-z]/;

  const apellidosApe = $("#apellidosApe").val();
  const apellidosMate = $("#apellidosMate").val();
  const nombresApo = $("#nombresApo").val();
  const dni = $("#dni").val();
  const gradoInstgruccion = $("#gradoInstgruccion").val();
  const distrito = $("#distrito").val();
  const direccion = $("#direccion").val();
  const sector = $("#sector").val();
  const celular = $("#celular").val();
  const email = $("#email").val();
  const sexo = $("#sexo").val();

  const frmRegistra = $("#frmRegistra").serialize();

  if (apellidosApe == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese el apellido paterno del apoderado!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (apellidosMate == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese el apellido materno del apoderado!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (nombresApo == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese le nombre apoderado!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (!expdni.test(dni)) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese el DNI!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (gradoInstgruccion == 0 || gradoInstgruccion == null) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Seleccione el grado de instruccion!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (distrito == 0 || distrito == null) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Seleccione el distrito!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (direccion == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese la dirección!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (sector == "") {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese el sector!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (!expreCelu.test(celular)) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese el celular!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (!expreEmail.test(email)) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Ingrese el Email!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else if (sexo == 0 || sexo == null) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: "Seleccione le sexo!",
      confirmButtonText: "Aceptar!"
    });
    return false;
  } else {
    $.ajax({
      type: "POST",
      url: "funciones/registrar.php",
      data: frmRegistra,
      success: function(r) {
        if (r == 2) {
          Swal.fire({
            type: "error",
            title: "Error",
            text: "apoderado ya existe",
            confirmButtonText: "Aceptar!"
          });
        } else if (r == 1) {
          Swal.fire({
            position: "top-end",
            type: "success",
            title: "Se registro con exito",
            showConfirmButton: false,
            timer: 1500
          });
          document.getElementById("frmRegistra").reset();
        } else {
          Swal.fire({
            type: "error",
            title: "Error",
            text: "Error al guardar",
            confirmButtonText: "Aceptar!"
          });
        }
      }
    });
  }
}
